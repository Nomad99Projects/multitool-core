package com.igor.monomultitool.configuration;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.not;

@Configuration
@EnableSwagger2
@SuppressWarnings("Guava")
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(getPathsConfig())
            .build();

    }

    private Predicate<String> getPathsConfig() {
        return not(getNotAvailablePaths());
    }

    private Predicate<String> getNotAvailablePaths() {
        return
            Predicates.or(
                PathSelectors.regex("/actuator.*"),
                PathSelectors.regex("/error"),
                PathSelectors.regex("/rabbit.*")
            );
    }
}
