package com.igor.monomultitool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultitoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultitoolApplication.class, args);
    }

}
